/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SD_IPV4_ENDPOINT_OPTION_H
#define SD_IPV4_ENDPOINT_OPTION_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"
#include "ByteStream.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

// See [TR_SOMEIP_00309]
class SDIPv4EndpointOption
{
public:
	static const int TCP = 0x06;
	static const int UDP = 0x11;

	ByteStream serialize()
	{
		ByteStream b;
		b.serializeShort(Length);
		b.serializeByte(type);
		b.serializeByte(0x00);
		b.serializeInt(IPv4Address);
		b.serializeByte(0x00);
		b.serializeByte(TransportProtocol);
		b.serializeShort(TransportProtocolPortNumber);

		return b;
	}

	uint32_t deserialize(ByteStream& b)
	{
		Length = b.deserializeShort();
		type = b.deserializeByte();
		b.deserializeByte();
		IPv4Address = b.deserializeInt();
		b.deserializeByte();
		TransportProtocol = b.deserializeByte();
		TransportProtocolPortNumber = b.deserializeShort();

		return b.getOffset();
	}

	String toString()
	{
		return String("");
		/*
		return "Service Option " + "Type: " + (int) (0xFF & type) + ", IPv4Address: " 
				+ intToIP(IPv4Address) + "" 
				+ ", TransportProtocolPortNumber: " + TransportProtocolPortNumber 
				+ ", TransportProtocol: " + TransportProtocol;
		*/
	}

	int getIPv4Address()
	{
		return IPv4Address;
	}

	StringView getIPv4AddressString()
	{
		uint32_t in = htonl(IPv4Address);
		String ret(INET_ADDRSTRLEN, '\0');
		inet_ntop(AF_INET, &in, &ret[0], ret.size());
		return ret;
	}
	
	void setIPv4Address(String sIPv4Address)
	{
		uint32_t ret;
		inet_pton(AF_INET, sIPv4Address.c_str(), &ret);
		IPv4Address = ntohl(ret);
	}
	
	void setIPv4Address(uint32_t IPv4Address)
	{
		this->IPv4Address = IPv4Address;
	}
	
	uint8_t getTransportProtocol()
	{
		return TransportProtocol;
	}

	void setTransportProtocol(uint8_t transportProtocol)
	{
		this->TransportProtocol = transportProtocol;
	}

	uint16_t getTransportProtocolPortNumber()
	{
		return TransportProtocolPortNumber;
	}

	void setTransportProtocolPortNumber(uint16_t transportProtocolPortNumber)
	{
		this->TransportProtocolPortNumber = transportProtocolPortNumber;
	}

private:
	Logger& logger = CreateLogger (StringView(""), StringView(""));

	// Shall be set to 0x0009.
	uint16_t Length;

	// Shall be set to 0x24.
	uint8_t type;

	// Shall transport the unicast IP-Address of SOME/IP as
	// four Bytes.
	uint32_t IPv4Address;

	// Shall be set to the transport layer protocol
	// (ISO/OSI layer 4) based on the IANA/IETF types (0x06: TCP, 0x11: UDP).
	uint8_t TransportProtocol;

	// Shall be set to the transportlayer port of SOME/IP-SD (e.g. 30490).
	uint16_t TransportProtocolPortNumber;
};

}

#endif
