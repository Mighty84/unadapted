/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SD_SERVICE_ENTRY_H
#define SD_SERVICE_ENTRY_H

#include "ByteStream.h"

namespace ara::com::someip {

class SDServiceEntry
{
public:
	// ServiceEntryType [PRS_SOMEIPSD_00268]
	enum ServiceEntryType {
		FIND_SERVICE = 0x00,
		OFFER_SERVICE = 0x01
	};

	ByteStream serialize()
	{
		ByteStream b;
		b.serializeByte(type);
		b.serializeByte(IndexFirstOptionRun);
		b.serializeByte(IndexSecondOptionRun);
		b.serializeByte((uint8_t) (NumberOfOptions1 & (NumberOfOptions2 << 4)));
		b.serializeShort(serviceId);
		b.serializeShort(instanceId);

		b.serializeByte(MajorVersion);

		// TTL
		int TTLa, TTLb, TTLc;
		b.serializeByte(TTLa);
		b.serializeByte(TTLb);
		b.serializeByte(TTLc);
		TTL = (TTLa << 16) | (TTLb << 8) | TTLc;

		b.serializeInt(MinorVersion);

		return b;
	}

	uint32_t deserialize(ByteStream& b)
	{
		type = b.deserializeByte();
		IndexFirstOptionRun = b.deserializeByte();
		IndexSecondOptionRun = b.deserializeByte();

		uint8_t NumberOfOptions = b.deserializeByte();
		NumberOfOptions1 = (uint8_t) (NumberOfOptions & 0x0F);
		NumberOfOptions2 = (uint8_t) ((NumberOfOptions & 0x0F) >> 4);

		serviceId = b.deserializeShort();
		instanceId = b.deserializeShort();

		uint32_t tmp = b.deserializeInt();
		MajorVersion = (uint8_t) ((tmp & 0xF000) >> 24);
		TTL = (uint8_t) (tmp & 0x0FFF);

		MinorVersion = b.deserializeInt();

		return b.getOffset();
	}

	String toString()
	{
		stringstream ss;
		ss << "Service Entry " << "Type: " << (int) type << ", ServiceId: "
			<< serviceId << ", InstanceId: " << instanceId;
		return ss.str();
	}

	uint16_t getServiceId()
	{
		return serviceId;
	}

	void setServiceId(uint16_t serviceId)
	{
		this->serviceId = serviceId;
	}

	uint8_t getServiceType()
	{
		return type;
	}

	void setServiceType(uint8_t serviceType)
	{
		this->type = serviceType;
	}

private:
	// encodes FindService (0x00) and OfferService (0x01).
	uint8_t type;

	// Index of this runs first option in the option array.
	uint8_t IndexFirstOptionRun; 

	// Index of this runs second option in the option array.
	uint8_t IndexSecondOptionRun;

	// Describes the number of options the first option run uses.
	uint8_t NumberOfOptions1; // actually a nibble
	
	// Describes the number of options the second option run uses.
	uint8_t NumberOfOptions2; // actually a nibble

	// Describes the Service ID of the Service or Service-Instance this entry is concerned with.
	uint16_t serviceId = 0;

	// Describes the Service Instance ID of the Service Instance
	// this entry is concerned with or is set to 0xFFFF if all service instances of a service
	// are meant.
	uint16_t instanceId = 0;

	// Encodes the major version of the service (instance).
	uint8_t MajorVersion;

	// Describes the lifetime of the entry in seconds.
	uint32_t TTL; // actually 24bit

	// Encodes the minor version of the service.
	uint8_t MinorVersion;
};

}

#endif
