/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SD_IPV6_ENDPOINT_OPTION_H
#define SD_IPV6_ENDPOINT_OPTION_H

namespace ara::com::someip {

// See [TR_SOMEIP_00318]
class SDIPv6EndpointOption
{
public:
	ByteStream serialize()
	{
		ByteStream b;
		b.serializeShort(Length);
		b.serializeByte(type);
		b.serializeByte(0x00);
		b.serializeBytes((char*) IPv6Address, sizeof(IPv6Address));
		b.serializeByte(0x00);
		b.serializeByte(TransportProtocol);
		b.serializeShort(TransportProtocolPortNumber);

		return b;
	}

	uint32_t deserialize(ByteStream& b)
	{
		Length = b.deserializeShort();
		type = b.deserializeByte();
		b.deserializeByte();
		IPv6Address = (uint64_t) b.deserializeBytes(16);
		b.deserializeByte();
		TransportProtocol = b.deserializeByte();
		TransportProtocolPortNumber = b.deserializeShort();

		return b.getOffset();
	}

	String toString()
	{
		return String("");
		/*
		return "Service Option " + "Type: " + (int) (0xFF & type) + ", IPv6Address: " 
				+ IPv6Address + "" 
				+ ", TransportProtocolPortNumber: " + TransportProtocolPortNumber 
				+ ", TransportProtocol: " + TransportProtocol;
		*/
	}

	uint64_t getIPv6Address()
	{
		return IPv6Address;
	}

	String getIPv6AddressString()
	{
		String ret(INET6_ADDRSTRLEN, '\0');
		inet_ntop(AF_INET6, &IPv6Address, &ret[0], ret.size());
		
		return ret;
	}
	
	void setIPv6Address(String IPv6Address)
	{

	}
	/*
	void setIPv6Address(char* IPv6Address) {
		this->IPv6Address = IPv6Address;
	}
	*/

	uint8_t getTransportProtocol()
	{
		return TransportProtocol;
	}

	void setTransportProtocol(uint8_t transportProtocol)
	{
		this->TransportProtocol = transportProtocol;
	}

	int getTransportProtocolPortNumber()
	{
		return TransportProtocolPortNumber;
	}

	void setTransportProtocolPortNumber(uint16_t transportProtocolPortNumber)
	{
		this->TransportProtocolPortNumber = transportProtocolPortNumber;
	}

private:
	Logger& logger = CreateLogger (StringView(""), StringView(""));

	// Shall be set to 0x0009.
	uint16_t Length;

	// Shall be set to 0x24.
	uint8_t type;

	// Shall transport the unicast IP-Address of SOME/IP as
	// eight bytes.
	uint64_t IPv6Address;

	// Shall be set to the transport layer protocol
	// (ISO/OSI layer 4) based on the IANA/IETF types (0x06: TCP, 0x11: UDP).
	uint8_t TransportProtocol;

	// Shall be set to the transportlayer port of SOME/IP-SD (e.g. 30490).
	uint32_t TransportProtocolPortNumber;
};

}

#endif
