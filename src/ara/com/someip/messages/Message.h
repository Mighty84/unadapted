/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include "ByteStream.h"

namespace ara::com::someip {

class Message
{
public:
	Message()
	{
		// Default initialization
		this->clientId = 0x01; // Needs to be unique (and apparently != 0 for vSomeIP)
		this->protocolVersion = 0x01; // Currently not used but must be 0x01
		this->interfaceVersion = (uint8_t) interfaceVersion;
	}

	String toString()
	{
		return String("");
		/*
		return Payload::toString() + "Message ServiceId: " + Integer.toHexString(0xFFFF & serviceId) + 
				", MethodId: " + Integer.toHexString(0xFFFF & methodId) 
				+ ", MessageType: " + Integer.toHexString(messageType) + "\n";
		*/
	}
	
	ByteStream serialize()
	{
		ByteStream b;
		b.setOffset(0);

		b.serializeShort(serviceId);
		b.serializeShort(methodId);

		// Save length offset as it's filled in later
		lengthOffset = b.getOffset();
		b.serializeInt(0);

		b.serializeShort(clientId);
		b.serializeShort(sessionId);
		b.serializeByte(protocolVersion);
		b.serializeByte(interfaceVersion);
		b.serializeByte(messageType);
		b.serializeByte(returnCode);

		b.serializeBytes(messagePayload, messagePayloadSize);

		uint32_t length = b.getOffset() - 8; // Omit first two ints [TR_SOMEIP_00042]

		// Patch the length field, then jump back to the end
		uint32_t end = b.getOffset();
		b.setOffset(lengthOffset);
		b.serializeInt(length);
		b.setOffset(end);

		return b;
	}

	uint32_t deserialize(char* ndata, uint32_t nsize)
	{
		ByteStream b(ndata, nsize);

		serviceId = b.deserializeShort();
		methodId = b.deserializeShort();
		length = b.deserializeInt();
		clientId = b.deserializeShort();
		sessionId = b.deserializeShort();
		protocolVersion = b.deserializeByte();
		interfaceVersion = b.deserializeByte();
		messageType = b.deserializeByte();
		returnCode = b.deserializeByte();

		// Copy the rest of the message, excluding the
		// Header into our own buffer
		messagePayloadSize = nsize - b.getOffset();
		messagePayload = new char[messagePayloadSize];
		memcpy(messagePayload, (void*) &ndata[b.getOffset()], messagePayloadSize);

		return b.getOffset();
	}

	void setServiceId(uint32_t serviceId)
	{
		this->serviceId = (short) serviceId;
	}
	

	uint16_t getServiceId()
	{
		return this->serviceId;
	}

	/*
	 * Used only for request/response.
	 * First bit set to zero indicates a method [TR_SOMEIP_00038]
	 */
	void setMethodId(uint16_t methodId)
	{
		// TODO Should mask highest bit (0xEFFF) but apparently
		// people also use the highest bit in method identifiers
		// -> tolerate abuse against specification
		this->methodId = (uint16_t)(methodId & 0xFFFF);
	}

	uint16_t getMethodId()
	{
		return this->methodId;
	}

	int getRequestId()
	{
		return (uint16_t) ((clientId << 16) | sessionId);
	}

	void setRequestId(uint16_t requestId)
	{
		this->clientId = (uint16_t) ((requestId >> 16) & 0xFFFF);
		this->sessionId = (uint16_t) (requestId & 0xFFFF);
	}
	
	/** 
	 * Setting the messageId indicates that you are not using
	 * request/response or publish/subscribe paradigms.
	 * [PRS_SOMEIP_00920] and [PRS_SOMEIP_00922] do state though,
	 * that these should be regerenced when constructing request/response
	 * messages.
	 */
	void setMessageId(uint32_t messageId)
	{
		this->serviceId = (uint16_t) (messageId >> 4);
		this->methodId = (uint16_t) (messageId & 0x0000FFFF);
	}

	uint32_t getMessageId()
	{
	    return (this->serviceId << 4) | this->methodId;
	}

	void setInterfaceVersion(uint8_t interfaceVersion)
	{
		this->interfaceVersion = interfaceVersion;
	}

	uint8_t getInterfaceVersion()
	{
		return this->interfaceVersion;
	}

	void setMessageType(uint8_t messageType)
	{
		this->messageType = messageType;
	}

	uint8_t getMessageType()
	{
		return this->messageType;
	}

	void setMessagePayload(char* payload, uint32_t payloadSize)
	{
		this->messagePayload = payload;
		this->messagePayloadSize = payloadSize;
	}

	char* getMessagePayload()
	{
		return this->messagePayload;
	}

	uint32_t getMessagePayloadSize()
	{
		return this->messagePayloadSize;
	}

	uint16_t getSessionId()
	{
		return sessionId;
	}

	// ReturnCodes [TR_SOMEIP_00191]
	enum ReturnCodes {
		E_OK = 0x00,
		E_NOT_OK = 0x01,
		E_UNKNOWN_SERVICE = 0x02,
		E_UNKNOWN_METHOD = 0x03,
		E_NOT_READY = 0x04,
		E_NOT_REACHABLE = 0x05,
		E_TIMEOUT = 0x06,
		E_WRONG_PROTOCOL_VERSION = 0x07,
		E_WRONG_INTERFACE_VERSION = 0x08,
		E_MALFORMED_MESSAGE = 0x09,
		E_WRONG_MESSAGE_TYPE = 0x0a
		// 0x0b - Reserved
	};

	void setReturnCode(uint8_t returnCode)
	{
		this->returnCode = returnCode;
	}

	// MessageType [TR_SOMEIP_00055]
	enum MessageType {
		REQUEST = 0x00,
		REQUEST_NO_RETURN = 0x01,
		NOTIFICATION = 0x02,
		REQUEST_ACK = 0x40,
		REQUEST_NO_RETURN_ACK = 0x41,
		NOTIFICATION_ACK = 0x42,
		RESPONSE = 0x80,
		ERROR = 0x81,
		RESPONSE_ACK = 0xC0,
		ERROR_ACK = 0xC1
	};

	static String getMessageTypeName(uint8_t messageType)
	{
		switch(messageType) {
		case MessageType::REQUEST:
			return "REQUEST";
		case MessageType::REQUEST_NO_RETURN:
			return "REQUEST_NO_RETURN";
		case MessageType::NOTIFICATION:
			return "NOTIFICATION";
		case MessageType::REQUEST_ACK:
			return "REQUEST_ACK";
		case MessageType::REQUEST_NO_RETURN_ACK:
			return "REQUEST_NO_RETURN_ACK";
		case MessageType::NOTIFICATION_ACK:
			return "NOTIFICATION_ACK";
		case MessageType::RESPONSE:
			return "RESPONSE";
		case MessageType::ERROR:
			return "ERROR";
		case MessageType::RESPONSE_ACK:
			return "RESPONSE_ACK";
		case MessageType::ERROR_ACK:
			return "ERROR_ACK";
		}

		return "Invalid messageType";
	}

	uint8_t getProtocolVersion()
	{
		return protocolVersion;
	}

	void setProtocolVersion(uint8_t protocolVersion)
	{
		this->protocolVersion = (uint8_t) protocolVersion;
	}

protected:
	void setLength(uint32_t i)
	{
		length = i;
	}

	/*
	 * Used only for publish/subscribe.
	 * First bit set to one indicates an event (Pub/Sub) [TR_SOMEIP_00040]
	 */
	void setEventId(uint16_t methodId)
	{
		this->methodId = (uint16_t)(methodId & 0xFFFF);
	}

	void setClientId(uint16_t clientId)
	{
		this->clientId = clientId;
	}

	void setSessionId(uint16_t sessionId)
	{
		this->sessionId = sessionId;
	}

private:
	int lengthOffset = 0;

	// Message header [TR_SOMEIP_00031]
	uint16_t serviceId; // TODO [TR_SOMEIP_00038]
	uint16_t methodId;
	uint32_t length;
	uint16_t clientId;
	uint16_t sessionId;
	uint8_t protocolVersion;
	uint8_t interfaceVersion;
	uint8_t messageType;
	uint8_t returnCode;

	char* messagePayload = NULL;
	uint32_t messagePayloadSize = 0;
};

}

#endif
