/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SD_MESSSAGE_H
#define SD_MESSSAGE_H

#include <sstream>
#include <assert.h>

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"

#include "SDIPv4EndpointOption.h"
#include "SDIPv6EndpointOption.h"
#include "SDServiceEntry.h"
#include "Message.h"
#include "ByteStream.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

// See [TR_SOMEIP_00251]
class SDMessage : public Message
{
public:
	// EntryType [TR_SOMEIP_00268]
	enum EntryType {
		FIND_SERVICE = 0x00,
		OFFER_SERVICE = 0x01,
		SUBSCRIBE = 0x06,
		SUBSCRIBE_ACK = 0x07
	};

	// OptionType
	enum OptionType {
		IPV4_ENDPOINT_OPTION = 0x04, // [TR_SOMEIP_00309]
		IPV6_ENDPOINT_OPTION = 0x06  // [TR_SOMEIP_00318]
	};

	// Transport protocols for the endpoint options ([TR_SOMEIP_00307] / [TR_SOMEIP_00316])
	static const int TCP = 0x06;
	static const int UDP = 0x11;

	SDMessage(uint16_t sessionId)
	{
		// See [TR_SOMEIP_00250]
		setServiceId(0xFFFF);
		setMethodId(0x8100);
		setProtocolVersion(0x01);
		setInterfaceVersion((uint8_t) 0x01);
		setMessageType(MessageType::NOTIFICATION);
		setReturnCode(0x00);
		setClientId(0);
		setSessionId(0);
		setSessionId(sessionId);
		setProtocolVersion(0x01);
	}

	static const int ENTRY_LENGTH_BYTES = 16;
	static const int OPTION_LENGTH_BYTES = 12;

	ByteStream serialize()
	{
		ByteStream b;
		b.serializeByte(flags);

		// Reserved bytes
		b.serializeByte(0);
		b.serializeByte(0);
		b.serializeByte(0);

		uint32_t entriesArraySize = entries.size() * ENTRY_LENGTH_BYTES;
		b.serializeInt(entriesArraySize);

		for(auto e : entries) {
			ByteStream eb = e.serialize();
			b.serializeBytes(eb);
		}

		uint32_t optionsArraySize = options.size() * OPTION_LENGTH_BYTES;
		b.serializeInt(optionsArraySize);

		for(auto o : options) {
			ByteStream ob = o.serialize();
			b.serializeBytes(ob);
		}

		setMessagePayload(b.getData(), b.getOffset());
		return Message::serialize();
	}

	uint32_t deserialize(char* ndata, uint32_t nsize)
	{
		Message::deserialize(ndata, nsize);

		ByteStream b(getMessagePayload(), getMessagePayloadSize());
		flags = b.deserializeByte();

		// Reserved bytes
		b.deserializeByte();
		b.deserializeByte();
		b.deserializeByte();

		// Read entries array
		uint32_t entriesArraySize = b.deserializeInt();
		logger.LogInfo() << "EntriesArraySize: " << entriesArraySize;

		for(int i = 0; i < entriesArraySize / ENTRY_LENGTH_BYTES; i++) {
			char* d = b.deserializeBytes(ENTRY_LENGTH_BYTES);
			ByteStream be(d, ENTRY_LENGTH_BYTES);
			SDServiceEntry sde;
			sde.deserialize(be);
			logger.LogInfo() << sde.toString();
			entries.push_back(sde);
		}

		// Read options array
		int optionsArrayLength = b.deserializeInt();
		logger.LogInfo() << "OptionsArrayLength: " << optionsArrayLength;

		for(int i = 0; i < optionsArrayLength / OPTION_LENGTH_BYTES; i++) {
			char* d = b.deserializeBytes(OPTION_LENGTH_BYTES);
			ByteStream bo(d, OPTION_LENGTH_BYTES);
			SDIPv4EndpointOption sdo;
			sdo.deserialize(bo);
			logger.LogInfo() << sdo.toString();
			options.push_back(sdo);
		}

		return b.getOffset();
	}


	String toString()
	{
		/*
		return super.toString() + "SDMessage Flags: " + (int) (0xFF & flags) + ", Entries: "
				+ entries.size() + ", Options: " + options.size() + "\n";
		*/
		return String("");
	}

	SDIPv4EndpointOption getIPv4EndpointOption()
	{
		assert(options.size() == 1);
		return options[0];
	}

	void addEntry(SDServiceEntry e)
	{
		entries.push_back(e);
	}

	void addOption(SDIPv4EndpointOption o)
	{
		options.push_back(o);
	}

private:
	Logger& logger = CreateLogger(StringView(""), StringView(""));

	// SD Message header [TR_SOMEIP_00251]
	uint8_t flags = 0;

	Vector<SDServiceEntry> entries;
	Vector<SDIPv4EndpointOption> options;
};

}

#endif
