/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef BYTESTREAM_H
#define BYTESTREAM_H

#include <sstream>
#include <string>

#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "ara/core/coretypes.h"

using namespace std;
using namespace ara::core;

namespace ara::com::someip {

class ByteStream
{
public:
	// See [TR_SOMEIP_00139]
	const int DEFAULT_MTU_SIZE = 1500;

	ByteStream()
	{
		allocatedData = true;
		data = (char*) malloc(DEFAULT_MTU_SIZE);
		dataSize = DEFAULT_MTU_SIZE;
	}

	ByteStream(char *ndata, uint32_t nsize)
	{
		data = ndata;
		dataSize = nsize;
	}

	~ByteStream()
	{
		if(allocatedData)
			free(data);
	}

	char* getData()
	{
		return data;
	}

	// The offset is used to indicate the
	// amound of valid data in the ByteStream.
	// Before changing it, you must save it
	// using getOffset.
	void setOffset(uint32_t offset)
	{
		if(offset + 1 > dataSize) {
			fprintf(stderr, "Cannot set offset out of bounds\n");
			assert(0);
		}

		this->offset = offset;
	}

	// Will return the position in the ByteStream
	// after serializing/deserializing data.
	// We assume, that after a sequence of
	// serialization/deserialization calls
	// that the offset will indicate the total
	// amount of valid data therein.
	int getOffset()
	{
		return offset;
	}

	void DuplicateArraySize()
	{
		if(!allocatedData) {
			fprintf(stderr, "Data was not allocated by ByteStream and cannot be duplicated\n");
			assert(0);
		}

		if(data == NULL) {
			fprintf(stderr, "DuplicateArraySize was called but ByteStream has no data\n");
			assert(0);
		}

		const int FACTOR = 2;
		char* tmp = (char*) malloc(dataSize*FACTOR);
		memcpy(tmp, data, dataSize);
		free(data);
		data = tmp;

		dataSize *= FACTOR;
	}

	String toString()
	{
		//stringstream str;
		//str << "Raw Data: " << data << "\n";
		//return str.str();
		return String("Data: ");
	}

	void serializeByte(uint8_t v)
	{
		if(offset >= dataSize)
			DuplicateArraySize();

		data[offset] = v;
		++offset;
	}

	void serializeShort(uint16_t v)
	{
		serializeByte((uint8_t) ((v & 0xFF00) >> 8));
		serializeByte((uint8_t) (v & 0x00FF));
	}

	void serializeInt(uint32_t v)
	{
		serializeByte((uint8_t) ((v & 0xFF000000) >> 24));
		serializeByte((uint8_t) ((v & 0x00FF0000) >> 16));
		serializeByte((uint8_t) ((v & 0x0000FF00) >> 8));
		serializeByte((uint8_t) (v & 0x000000FF));
	}

	void serializeLong(uint64_t v)
	{
		serializeInt((uint32_t)(v & 0x00000000FFFFFFFF));
		serializeInt((uint32_t)(v >> 32));
	}

	void serializeBytes(ByteStream& b)
	{
		serializeBytes(b.getData(), b.getOffset());
	}

	void serializeBytes(const char* ndata, uint32_t nsize)
	{
		if(ndata != NULL && nsize == 0) {
			fprintf(stderr, "Invalid data\n");
			assert(0);
		}

		while((offset + nsize) >= dataSize)
			DuplicateArraySize();

		memcpy((void*) &data[offset], ndata, nsize);
		offset += nsize;
	}

	uint8_t deserializeByte()
	{
		if(offset >= dataSize) {
			fprintf(stderr, "Cannot set deserialize byte out of bounds\n");
			assert(0);
		}

		uint8_t v = data[offset];
		++offset;

		return v;
	}

	uint16_t deserializeShort()
	{
		uint8_t v1 = deserializeByte();
		uint8_t v2 = deserializeByte();
		return (uint16_t) (((0xFF & v1) << 8) | (0xFF & v2));
	}

	uint32_t deserializeInt()
	{
		uint8_t v1 = deserializeByte();
		uint8_t v2 = deserializeByte();
		uint8_t v3 = deserializeByte();
		uint8_t v4 = deserializeByte();
		return (uint32_t) ((0xFF & v1) << 24) | ((0xFF & v2) << 16) | ((0xFF & v3) << 8) | (0xFF & v4);
	}

	uint64_t deserializeLong()
	{
		uint64_t a  = deserializeInt();
		uint64_t b  = deserializeInt();

		return a | (b << 32);
	}

	char* deserializeBytes(uint32_t size)
	{
		if((offset + size) > dataSize) {
			fprintf(stderr, "Cannot set deserialize bytes out of bounds\n");
			assert(0);
		}

		char *d = &data[offset];
		offset += size;
		return d;
	}

	void serializeTLV(const char* ndata, uint32_t nsize)
	{
		serializeInt(nsize);
		serializeBytes(ndata, nsize);
	}

	const char* deserializeTLV()
	{
		uint32_t nsize = deserializeInt();
		char* ndata = deserializeBytes(nsize);

		return ndata;
	}

	void serializeString(const string& str)
	{
		serializeInt(str.length());
		serializeBytes(str.c_str(), str.length());
	}

	const string deserializeString()
	{
		uint32_t nsize = deserializeInt();
		const char* ndata = deserializeBytes(nsize);
		string str(ndata, nsize);

		return str;
	}

	void deserializeSkip(uint32_t size)
	{
		offset += size;
	}

private:
	bool allocatedData = false;
	char* data = NULL;
	uint32_t dataSize;
	uint32_t offset = 0;
};

}

#endif
