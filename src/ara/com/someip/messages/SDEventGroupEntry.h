/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SD_EVENT_GROUP_ENTRY_H
#define SD_EVENT_GROUP_ENTRY_H

#include <sstream>

#include "ara/core/coretypes.h"

using namespace ara::core;

namespace ara::com::someip {

class SDEventGroupEntry
{
public:
	// ReturnCodes [TR_SOMEIP_00270]
	enum ReturnCodes {
		SUBSCRIBE = 0x06,
		SUBSCRIBE_ACK = 0x07
	};

	uint32_t serialize()
	{
		return 0;
	}

	uint32_t deserialize(char* ndata, uint32_t nsize)
	{
		ByteStream b(ndata, nsize);

		type = b.deserializeByte();
		firstOptionRun = b.deserializeByte();
		secondOptionRun = b.deserializeByte();

		uint8_t options = b.deserializeByte();
		options1 = (uint8_t) (options >> 4);
		options2 = (uint8_t) (options & 0x0F);

		serviceId = b.deserializeShort();
		instanceId = b.deserializeShort();

		uint32_t tmp1 = b.deserializeInt();
		major = (uint8_t) (tmp1 >> 24);
		TTL = tmp1 & 0x0FFF;

		uint16_t tmp2 = b.deserializeShort();
		counter = (uint8_t) (tmp2 & 0x000F);

		eventgroupId = b.deserializeShort();

		return b.getOffset();
	}

	String toString() {
		stringstream str;
		str << "Service Entry " << "Type: " << (int) type << ", ServiceId: "
			<< serviceId << ", InstanceId: " << instanceId;
		return str.str();
	}

private:
	//Type Field [uint8]: encodes Subscribe (0x06), and SubscribeAck (0x07).
	uint8_t type = 0;

	//Index First Option Run [uint8]: Index of this runs first option in the option array.
	uint8_t firstOptionRun;

	//Index Second Option Run [uint8]: Index of this runs second option in the option array.
	uint8_t secondOptionRun;

	//Number of Options 1 [uint4]: Describes the number of options the first option run	uses.
	uint8_t options1;

	// Number of Options 2 [uint4]: Describes the number of options the second option run uses.
	uint8_t options2;

	// Service-ID [uint16]: Describes the Service ID of the Service or Service Instance	this entry is concerned with.
	uint16_t serviceId = 0;

	// Instance ID [uint16]: Describes the Service Instance ID of the Service Instance
	// this entry is concerned with or is set to 0xFFFF if all service instances of a service
	// are meant.
	uint16_t instanceId = 0;

	// Major Version [uint8]: Encodes the major version of the service instance this
	// eventgroup is part of.
	uint8_t major = 0;

	//TTL [uint24]: Descibes the lifetime of the entry in seconds.
	uint32_t TTL = 0;

	// Reserved [uint12]: Shall be set to 0x000, if not specified otherwise (see
	// [TR_SOMEIP_00391] and [TR_SOMEIP_00394]).
	uint16_t reserved;

	// Counter [uint4]: Is used to differentiate identical Subscribe Eventgroups. Set to
	// 0x0 if not used.
	uint8_t counter = 0;

	// Eventgroup ID [uint16]: Transports the ID of an Eventgroup.
	uint16_t eventgroupId = 0;
};

}

#endif
