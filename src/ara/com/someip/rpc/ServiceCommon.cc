/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"

#include "ara/com/someip/messages/ByteStream.h"
#include "ara/com/someip/messages/Message.h"
#include "ara/com/someip/messages/SDMessage.h"
#include "ServiceProxy.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

int ServiceCommon::sendMessage(Message& m)
{
	ByteStream b = m.serialize();
	int txLen = b.getOffset();
	char *txBuf = b.getData();

	if(transportProtocol == SDMessage::UDP) {
		if(address.length() == 0) {
			logger.LogWarn() << "Service stub not found. The proxy cannot send to a stub without address.";
			return 0;
		}

		// See [TR_SOMEIP_00061]
		if(m.getMessagePayloadSize() > 1400)
			logger.LogWarn() << "For SOME/IP messages sent with the UDP transportProtocol, "
					<< "payload shall not be > 1400 uint8_ts. Behaviour undefined.";
 
		struct sockaddr_in servaddr;
		memset((char*)&servaddr, 0, sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_port = htons(port);
		inet_aton(address.c_str(), &servaddr.sin_addr);

		if(sendto(localSocket, txBuf, txLen, 0, (sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
			perror("sendto failed");
			return 0;
		}

		send(localSocket, txBuf, txLen, 0);
		logger.LogInfo() << "Sent a UDP packet with length: " << txLen;
		return 1;
	}

	if(transportProtocol == SDMessage::TCP) {
		if(address.size() == 0) {
			logger.LogWarn() << "Service stub not found. The proxy cannot connect to a stub without address.";
			return 0;
		}

		if(remoteSocket == -1) {
			if((remoteSocket = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
				logger.LogError() << "Creating socket failed";
				exit(EXIT_FAILURE);
			}

			struct sockaddr_in servaddr;
			memset((char*)&servaddr, 0, sizeof(servaddr));
			servaddr.sin_family = AF_INET;
			servaddr.sin_port = htons(port);
			inet_aton(address.c_str(), &servaddr.sin_addr);

			int ret = connect(remoteSocket, (sockaddr*) &servaddr, sizeof(servaddr));
			if(ret == -1) {
				logger.LogInfo() << "Cannot connect proxy to stub";
				return 0;
			}

			struct timeval timeout;
			timeout.tv_sec = 0;
			timeout.tv_usec = 50000; // 50ms

			setsockopt(remoteSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
			setsockopt(remoteSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
		}

		if(send(remoteSocket, txBuf, txLen, MSG_NOSIGNAL) == -1) {
			logger.LogInfo() << "Send failed";
			remoteSocket = -1;
			return 0;
		}

		logger.LogInfo() << "Sent a TCP packet with length: " << txLen;
		return 1;
	}

	logger.LogError() << "Unknown transport protocol";
	return 0;
}

int ServiceCommon::receiveMessage(Message *m, uint32_t waitTime)
{
	// Receive a message
	// Currently implemented blocking with timeout for both UDP and TCP ports
	char rxBuf[8192];
	ssize_t rxLen = 0;

	if(transportProtocol == SDMessage::UDP) {
		rxLen = recv(localSocket, rxBuf, sizeof(rxBuf),0);
		logger.LogInfo() << "Received an answer on bound UDP port with length: " << rxLen;
	}

	if(transportProtocol == SDMessage::TCP) {
		if(remoteSocket == -1) {
			struct sockaddr_in sin;
			unsigned int sin_len = sizeof(sockaddr);
			remoteSocket = accept(localSocket, (sockaddr*) &sin, &sin_len);

			// No pending incoming connections
			if(remoteSocket == -1)
				return 0;

			//logger.LogWarn() << "Proxy is not connected";
			//return;

			struct timeval timeout;
			timeout.tv_sec = 0;
			timeout.tv_usec = waitTime * 1000;

			if(setsockopt(remoteSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
				logger.LogError() << "Setting setsockopt SO_SNDTIMEO on localsocket failed";
			if(setsockopt(remoteSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
				logger.LogError() << "Setting setsockopt SO_SNDTIMEO on localsocket failed";

			//int i = 1;
			//setsockopt(localSocket, IPPROTO_TCP, TCP_NODELAY, (void *)&i, sizeof(i)); // See [TR_SOMEIP_00148]
		}

		rxLen = recv(remoteSocket, rxBuf, sizeof(rxBuf), 0);
		if(rxLen < 0) {
			// Error
			return 0;
		}

		logger.LogInfo() << "Received an answer on bound TCP port with length: " << rxLen;
	}

	m->deserialize(rxBuf, rxLen);
	return 1;
}

}