/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"
#include "ara/com/someip/messages/Message.h"
#include "ara/com/someip/messages/SDMessage.h"
#include "ServiceStub.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

ServiceStub::ServiceStub(String address, uint16_t port, uint16_t serviceId,
	uint8_t interfaceVersion, uint8_t transportProtocol)
{
	this->address = address;
	this->port = port;
	this->serviceId = serviceId;
	this->interfaceVersion = interfaceVersion;
	this->transportProtocol = transportProtocol;

	logger.LogInfo() << "Creating ServiceStub socket on port " << port;

	// Create either a UDP or a TCP socket
	if(transportProtocol == SDMessage::UDP) {
		if((localSocket = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
			logger.LogError() << "Creating socket failed";
			exit(EXIT_FAILURE);
		}
	}

	if(transportProtocol == SDMessage::TCP) {
		if((localSocket = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
			logger.LogError() << "Creating socket failed";
			exit(EXIT_FAILURE);
		}

		// Don't linger
		// Let previous bind of socket die gracefully but bind anyways
		int reuseaddr = 1;
		if(setsockopt(localSocket, SOL_SOCKET,
			SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) < 0) {

			perror("setsockopt:SO_REUSEADDR");
			exit(EXIT_FAILURE);
		}
	}

	struct sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	//sin.sin_addr.s_addr = htonl(INADDR_ANY); // Bind to all interfaces
	sin.sin_addr.s_addr = inet_addr(address.c_str()); // Bind to user set address
	sin.sin_port = htons(port);

	if(bind(localSocket, (sockaddr*) &sin, sizeof(sockaddr_in)) < 0)
		logger.LogError() << "Binding socket failed";

	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 50000; // 50ms
	if(setsockopt(localSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
		logger.LogError() << "Setting setsockopt SO_RCVTIMEO on localsocket failed";

	if(setsockopt(localSocket, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
		logger.LogError() << "Setting setsockopt SO_SNDTIMEO on localsocket failed";

	listen(localSocket, 5);

	logger.LogInfo() << "ServiceStub initialized";
}

void ServiceStub::handleMessages()
{
	Message m;
	if(!receiveMessage(&m))
		return; // Nothing received

	ByteStream payload(m.getMessagePayload(), m.getMessagePayloadSize());
	ByteStream result;
	handleMessage(m.getMessageType(), m.getMethodId(), payload, result);

	// Send response if the message was a request with response
	if(m.getMessageType() == Message::REQUEST) {
		Message r;
		r.setMessageId(m.getMessageId());
		r.setProtocolVersion(m.getProtocolVersion());
		r.setInterfaceVersion(m.getProtocolVersion());
		r.setMessageType(Message::RESPONSE);
		r.setReturnCode(Message::E_OK);
		r.setMessagePayload(result.getData(), result.getOffset());

		if(transportProtocol == SDMessage::UDP) {
			sendMessage(r);
		}

		if(transportProtocol == SDMessage::TCP) {
			sendMessage(r);
		}
	}
}

}
