/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SERVICE_COMMON_H
#define SERVICE_COMMON_H

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"
#include "ara/com/someip/messages/Message.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

class ServiceCommon
{
public:
	String getAddress()
	{
		return this->address;
	}

	int getPort()
	{
		return this->port;
	}

	void setAddress(String address, int port)
	{
		this->address = address;
		this->port = port;
	}

	uint16_t getServiceId()
	{
		return this->serviceId;
	}

	uint8_t getInterfaceVersion()
	{
		return this->interfaceVersion;
	}

	uint8_t getTransportProtocol()
	{
		return transportProtocol;
	}

protected:
	Logger& logger = CreateLogger(StringView(""), StringView(""));

	String address = "";
	uint16_t port = 0;
	uint16_t serviceId = 0;
	uint8_t interfaceVersion = 0;
	uint8_t transportProtocol = 0;

	int receiveMessage(Message *m, uint32_t waitTime = 50); // Default wait time is 50ms
	int sendMessage(Message& m);

	int localSocket = -1;
	int remoteSocket = -1;

};

}

#endif