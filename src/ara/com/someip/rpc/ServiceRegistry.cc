/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <chrono>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include "ara/core/coretypes.h"
#include "ara/com/someip/messages/SDMessage.h"
#include "ara/com/someip/messages/SDEventGroupEntry.h"
#include "ara/com/someip/messages/SDIPv4EndpointOption.h"
#include "ara/com/someip/messages/SDIPv6EndpointOption.h"
#include "ara/com/someip/messages/SDServiceEntry.h"
#include "ServiceStub.h"
#include "ServiceRegistry.h"

using namespace ara::core;

namespace ara::com::someip {

int ServiceRegistry::setupMulticastGroupSocket(const char* hostaddr, uint16_t port)
{
	struct ip_mreq command;

	int socket_descriptor;
	struct sockaddr_in sin;
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(port);

	if((socket_descriptor = socket(PF_INET,
		SOCK_DGRAM, 0)) == -1) {

		perror("socket()");
		exit(EXIT_FAILURE);
	}

	// Don't block on recv
	struct timeval read_timeout;
	read_timeout.tv_sec = 0;
	read_timeout.tv_usec = 10;
	setsockopt(socket_descriptor, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);

	// Allow multiple processes to use the port
	int reuseaddr = 1;
	if(setsockopt(socket_descriptor,
		SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)) < 0) {

		perror("setsockopt:SO_REUSEADDR");
		exit(EXIT_FAILURE);
	}

	// Enable broadcast loopback
	// This is only needed if we're sending from this socket and would also like to
	// receive the sent message
#if 1
	int loop = 1;
	if(setsockopt(socket_descriptor, IPPROTO_IP,
		IP_MULTICAST_LOOP, &loop, sizeof(loop)) < 0) {

		logger.LogError() << "setsockopt:IP_MULTICAST_LOOP";
		exit(EXIT_FAILURE);
	}
#endif

	if(bind(socket_descriptor, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
		logger.LogError() << "Binding socket failed";
		exit(EXIT_FAILURE);
	}

	// Join the broadcast group
	command.imr_multiaddr.s_addr = inet_addr(hostaddr);
	command.imr_interface.s_addr = htonl(INADDR_ANY);
	if(command.imr_multiaddr.s_addr == -1) {
		logger.LogError() << "Supplied hostaddr is not a multicast address";
		exit(EXIT_FAILURE);
	}

	if(setsockopt(socket_descriptor, IPPROTO_IP,
		IP_ADD_MEMBERSHIP, &command, sizeof(command)) < 0) {

		logger.LogError() << "setsockopt:IP_ADD_MEMBERSHIP";
		exit(EXIT_FAILURE);
	}

	logger.LogInfo() << "Successfully acquired multicast group socket";

	return socket_descriptor;
}

ServiceRegistry::ServiceRegistry(const StringView& address, uint16_t port, uint32_t promoteInterval)
	: multicastAddress(address), multicastPort(port), promoteInterval(promoteInterval)
{
	sd_socket = setupMulticastGroupSocket(multicastAddress.c_str(), multicastPort);
}

#define RECVBUFSIZE 8192

bool ServiceRegistry::handleServiceDiscoveryMessage()
{
	char rxBuf[RECVBUFSIZE];
	sockaddr_in remaddr;
	socklen_t addrlen = sizeof(remaddr);
	ssize_t rxLen = recvfrom(sd_socket, rxBuf, RECVBUFSIZE, 0, (sockaddr *) &remaddr, &addrlen);
	if(rxLen <= 0)
		return false;

	logger.LogInfo() << "Received a packet with length: " << rxLen;

	SDMessage sdm(1);
	sdm.deserialize(rxBuf, rxLen);
	SDIPv4EndpointOption sdo = sdm.getIPv4EndpointOption();

	logger.LogInfo() << "Attempting to register service for address: " << sdo.getIPv4AddressString()
		<< ", port: " << sdo.getTransportProtocolPortNumber()
		<< ", serviceId: " << sdm.getServiceId()
		<< ", interfaceVersion: " << sdm.getInterfaceVersion()
		<< ", transportProtocol: " << sdo.getTransportProtocol();

	// Find proxy
	for(ServiceProxy* s : serviceProxyList) {
		if(s->getServiceId() == sdm.getServiceId()
			&& s->getTransportProtocol() == sdo.getTransportProtocol()
			&& s->getInterfaceVersion() == sdm.getInterfaceVersion()) {

			logger.LogInfo() << "Registered Service";

			s->setAddress(sdo.getIPv4AddressString(), sdo.getTransportProtocolPortNumber());
			s->setConnectionState(true);
			return true;
		}
	}

	// No proxy to handle service registered
	logger.LogInfo() << "No service proxy registered to interface with the offered service";
	return false;
}

void ServiceRegistry::promoteService(ServiceStub& service)
{
	SDMessage sdm(service.getPort());
	sdm.setServiceId(service.getServiceId());
	sdm.setInterfaceVersion(service.getInterfaceVersion());

	SDServiceEntry sde;
	sde.setServiceId(service.getServiceId());
	sde.setServiceType(SDServiceEntry::OFFER_SERVICE);

	SDIPv4EndpointOption sdip;
	sdip.setIPv4Address(service.getAddress());
	sdip.setTransportProtocolPortNumber(service.getPort());
	sdip.setTransportProtocol(service.getTransportProtocol());

	sdm.addEntry(sde);
	sdm.addOption(sdip);
	ByteStream sdmb = sdm.serialize();

	struct sockaddr_in servaddr;
	memset((char*)&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(multicastPort);
	inet_aton(multicastAddress.c_str(), &servaddr.sin_addr);

	if(sendto(sd_socket, sdmb.getData(), sdmb.getOffset(), 0, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) {
		logger.LogError() << "sendto failed";
		return;
	}

	logger.LogInfo() << "Promoted service with serviceId: " << service.getServiceId()
		<< ", interfaceVersion: " << service.getInterfaceVersion();
}

void ServiceRegistry::registerService(ServiceProxy& service)
{
	for(ServiceProxy* s : serviceProxyList) {
		if(s->getServiceId() == service.getServiceId()
			|| s->getInterfaceVersion() == service.getInterfaceVersion()) {

			if(s->getAddress() != service.getAddress()) {
				s->setAddress(service.getAddress(), service.getPort());

				logger.LogInfo() << "Updated service proxy address for serviceId: " << service.getServiceId()
					<< ", interfaceVersion: " << service.getInterfaceVersion();
			}

			return;
		}
	}

	logger.LogInfo() << "Registered a service proxy for serviceId: " << service.getServiceId()
		<< ", interfaceVersion: " << service.getInterfaceVersion() 
		<< ", transportProtocol: " << service.getTransportProtocol();
	
	serviceProxyList.push_back(&service);
}

void ServiceRegistry::registerService(ServiceStub& service)
{
	for(ServiceStub* s : serviceStubList) {
		if(s->getServiceId() == service.getServiceId()
				|| s->getAddress() == service.getAddress()
				|| s->getInterfaceVersion() == service.getInterfaceVersion()) {

			logger.LogInfo() << "Known service not reregistered";
			return;
		}
	}
	
	logger.LogInfo() << "Registered a service stub for serviceId: " << service.getServiceId()
		<< ", interfaceVersion: " << service.getInterfaceVersion()
		<< ", transportProtocol: " << service.getTransportProtocol();

	serviceStubList.push_back(&service);

	// Promote the service right away
	promoteService(service);
}

void ServiceRegistry::update()
{
	// Handle all incoming service-discovery messages
	// until there are none left
	while(handleServiceDiscoveryMessage());

	std::chrono::steady_clock::time_point dt = std::chrono::steady_clock::now();
	uint32_t deltaMS = std::chrono::duration_cast<std::chrono::milliseconds>(dt - lastPromote).count();
	if(deltaMS > promoteInterval) {
		lastPromote = std::chrono::steady_clock::now();

		// Now promote the services we have in our registry
		for(ServiceStub* s : serviceStubList)
			promoteService(*s);
	}
}

}