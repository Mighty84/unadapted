/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SERVICE_PROXY_H
#define SERVICE_PROXY_H

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"

#include "ara/com/someip/messages/ByteStream.h"
#include "ara/com/someip/messages/SDMessage.h"
#include "ServiceCommon.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

class ServiceProxy : public ServiceCommon
{
public:
	ServiceProxy(uint16_t serviceId, uint8_t interfaceVersion, uint8_t transportProtocol);
	bool isConnected()
	{
		return connected;
	}

	void setConnectionState(bool connected)
	{
		this->connected = connected;

		logger.LogInfo() << "Updated connection state connected: " << connected;
	}

	bool getConnectionState()
	{
		return connected;
	}

protected:
	void callServiceRequestNoReturn(uint16_t serviceId, uint16_t methodId, ByteStream& i);
	int callServiceRequest(uint16_t serviceId, uint16_t methodId, ByteStream& i, ByteStream *r);

private:
	bool connected = false;
};

}

#endif
