/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SERVICE_REGISTRY_H
#define SERVICE_REGISTRY_H

#include <chrono>

#include "ara/core/coretypes.h"
#include "ServiceStub.h"
#include "ServiceProxy.h"

using namespace ara::core;

namespace ara::com::someip {

class ServiceRegistry
{
public:
	ServiceRegistry(const StringView& multicastGroup, uint16_t port, uint32_t promoteInterval);

	bool handleServiceDiscoveryMessage();
	void promoteService(ServiceStub& service);
	void registerService(ServiceProxy& service);
	void registerService(ServiceStub& service);

	void update();

private:
	Logger& logger = CreateLogger(StringView(""), StringView(""));

	int setupMulticastGroupSocket(const char* hostaddr, uint16_t port);

	Vector<ServiceStub*> serviceStubList;
	Vector<ServiceProxy*> serviceProxyList;

	String multicastAddress;
	int multicastPort;
	uint32_t sd_socket = 0;

	int promoteInterval;
	std::chrono::steady_clock::time_point lastPromote = std::chrono::steady_clock::now();
};

}

#endif
