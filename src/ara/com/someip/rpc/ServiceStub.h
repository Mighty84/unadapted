/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef SERVICE_STUB_H
#define SERVICE_STUB_H

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"
#include "ara/com/someip/messages/Message.h"
#include "ServiceCommon.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

class ServiceStub : public ServiceCommon
{
public:
	ServiceStub(String address, uint16_t port, uint16_t serviceId, uint8_t interfaceVersion, uint8_t transportProtocol);
	void handleMessages();

protected:
	virtual void handleMessage(uint8_t messageType, uint16_t methodId, ByteStream &payload, ByteStream &result) = 0;
};

}

#endif
