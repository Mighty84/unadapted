/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"

#include "ara/com/someip/messages/ByteStream.h"
#include "ara/com/someip/messages/Message.h"
#include "ara/com/someip/messages/SDMessage.h"
#include "ServiceProxy.h"

using namespace ara::core;
using namespace ara::log;

namespace ara::com::someip {

ServiceProxy::ServiceProxy(uint16_t serviceId, uint8_t interfaceVersion, uint8_t transportProtocol)
{
	this->serviceId = serviceId;
	this->interfaceVersion = interfaceVersion;
	this->transportProtocol = transportProtocol;

	if(transportProtocol == SDMessage::UDP)
		localSocket = socket(AF_INET, SOCK_DGRAM, 0);
}


void ServiceProxy::callServiceRequestNoReturn(uint16_t serviceId, uint16_t methodId, ByteStream& i)
{
	// Generate the call message
	Message m;
	m.setServiceId(serviceId);
	m.setMethodId(methodId);
	m.setMessageType(Message::REQUEST_NO_RETURN);
	m.setMessagePayload(i.getData(), i.getOffset());
	if(!sendMessage(m)) {
		logger.LogWarn() << "Cannot send request";
		return;
	}
}

#define WAIT_TIME_MS 150

int ServiceProxy::callServiceRequest(uint16_t serviceId, uint16_t methodId, ByteStream& i, ByteStream *res)
{
	// Generate the call message
	Message m;
	m.setServiceId(serviceId);
	m.setMethodId(methodId);
	m.setMessageType(Message::REQUEST);
	m.setMessagePayload(i.getData(), i.getOffset());
	if(!sendMessage(m)) {
		logger.LogWarn() << "Cannot send request";
		return 0;
	}

	// Wait for a response
	Message r;
	if(receiveMessage(&r, WAIT_TIME_MS)) {
		logger.LogInfo() << "Received response";
		// TODO really not the best way to copy the data into res
		res->serializeBytes(r.getMessagePayload(), r.getMessagePayloadSize());
		res->setOffset(0);
		return 1;
	} else {
		logger.LogError() << "Nothing received";
		return 0;
	}
}

}
