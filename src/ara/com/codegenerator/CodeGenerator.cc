/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "rapidjson/document.h"
#include "rapidjson/reader.h"
#include "rapidjson/error/en.h"
#include "rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

// Transport protocols for the endpoint options ([TR_SOMEIP_00307] / [TR_SOMEIP_00316])
const int TCP = 0x06;
const int UDP = 0x11;

static stringstream serviceDataStructuresCode;
static stringstream proxyConstructor;
static stringstream stubConstructor;
static stringstream stubMethodEnumeration;
static stringstream stubMethodAbsFunc;
static stringstream proxyMethodHandler;
static stringstream stubMethodHandler;
static stringstream implMethodHandler;
static stringstream proxyClass;
static stringstream stubClass;
static stringstream implClass;
static string serviceName;
static int serviceId;
static int interfaceVersion;

const string t1 = "	";
const string t2 = "		";
const string t3 = "			";

static void parseServiceAttributes(Document& d)
{
	serviceName = d["serviceName"].GetString();
	serviceId = d["serviceId"].GetInt();
	interfaceVersion = d["interfaceVersion"].GetInt();

	// Parse service attributes
	cout << "Processing service: " << serviceName << " with id: "
		<< serviceId << " and interface version: " << interfaceVersion << endl;

	string tpString = d["transportProtocol"].GetString();

	proxyConstructor << t1 << serviceName << "Proxy() : ServiceProxy(" << serviceId
		<< ", 0, SDMessage::" << tpString << ")\n"
		<< t1 << "{\n" << t1 << "}\n";

	stubConstructor << t1 << serviceName << "Stub() : ServiceStub(\"127.0.0.1\", "
		<< serviceId << ", " << serviceId << ", 0, SDMessage::" << tpString << ")\n"
		<< t1 << "{\n" << t1 << "}\n";

	return;
}

static string parseServiceDataStructureRec(Value& element, int rec)
{
	stringstream serviceDataStructures;

	// Spacing tabs for formatting
	stringstream sspacing;
	for(int i = 0; i < rec; i++)
		sspacing << "	";
	string spacing = sspacing.str();

	// Structure always has a type and name
	string type = element["type"].GetString();
	string name = element["name"].GetString();

	// and optional variables or further substructures
	if(type.compare("struct") != 0) {
		serviceDataStructures << spacing << type << " " << name << ";\n";
	}
	else {
		serviceDataStructures << spacing << "struct " << name << " {\n";

		if(element.HasMember("elements")) {
			for(auto& m : element["elements"].GetArray()) {
				serviceDataStructures << parseServiceDataStructureRec(m, rec+1);
			}
		}

		serviceDataStructures << spacing << "};\n";
	}

	return serviceDataStructures.str();
}

static void parseServiceDataStructures(Document& d)
{
	// Parse data structures
	for(auto& m : d["structures"].GetArray())
		serviceDataStructuresCode << parseServiceDataStructureRec(m, 0);

	return;
}

static stringstream buildSerializeCall(string obj, string type, string name)
{
	stringstream r;

	if(type == "int8_t" || type == "uint8_t")
		r << obj << ".serializeByte(" << name << ");\n";

	if(type == "int16_t" || type == "uint16_t")
		r << obj << ".serializeShort(" << name << ");\n";

	if(type == "int32_t" || type == "uint32_t")
		r << obj << ".serializeInt(" << name << ");\n";

	if(type == "int64_t" || type == "uint64_t")
		r << obj << ".serializeLong(" << name << ");\n";

	if(type == "string")
		r << obj << ".serializeString(" << name << ");\n";

	return r;
}

static stringstream buildDeserializeCall(string obj, string type, string name)
{
	stringstream r;

	if(type == "int8_t" || type == "uint8_t")
		r << "char " << name << " = " << obj << ".deserializeByte();\n";

	if(type == "int16_t" || type == "uint16_t")
		r << "short " << name << " = " << obj << ".deserializeShort();\n";

	if(type == "int32_t" || type == "uint32_t")
		r << "int " << name << " = " << obj << ".deserializeInt();\n";

	if(type == "int64_t" || type == "uint64_t")
		r << "long " << name << " = " << obj << ".deserializeLong();\n";

	if(type == "string")
		r << "string " << name << "(" << obj << ".deserializeString());\n";

	return r;
}

static void buildProxyMessageHandlers(Document& d)
{
	for(auto& m : d["methods"].GetArray()) {
		string methodName = m["name"].GetString();
		string callType = m["callType"].GetString();

		// Create the method parameter list
		stringstream methodParameterList;

		bool first = true;
		for(auto& n : m["parameters"].GetArray()) {

			if(first)
				first = false;
			else
				methodParameterList << ", ";

			string type = n["type"].GetString();
			string name = n["name"].GetString();

			methodParameterList << type << " " << name;
		}

		// Get the return type
		string returnType = "void";
		if(m.HasMember("returnType"))
			returnType = m["returnType"].GetString();

		// Now build the handler for this method
		proxyMethodHandler
			<< t1 << "std::future<" << returnType << "> " << methodName << "(" << methodParameterList.str() << ")\n"
			<< t1 << "{\n"
			<< t2 << "if(!isConnected()) {\n"
			<< t3 << "logger.LogWarn() << \"Cannot call the function as the proxy is not connected to the service stub\";\n"
			<< t2 << "}\n\n";

		proxyMethodHandler << t2 << "auto future = std::async([&] {\n";

		// Serialize parameters for remote method call
		proxyMethodHandler << t3 << "ByteStream b;\n";

		for(auto& n : m["parameters"].GetArray()) {
			string type = n["type"].GetString();
			string name = n["name"].GetString();

			proxyMethodHandler << t3 << buildSerializeCall("b", type, name).str();
		}

		// Call remote method
		if(callType == "REQUEST_NO_RETURN")
			proxyMethodHandler << t3 << "callServiceRequestNoReturn(serviceId, "
				<< serviceName << "Stub::METHOD_"
				<< methodName << "_CALL, b);\n";

		if(callType == "REQUEST") {
			proxyMethodHandler << t3 << "ByteStream r;\n";
			proxyMethodHandler << t3 << "callServiceRequest(serviceId, "
				<< serviceName << "Stub::METHOD_"
				<< methodName << "_CALL, b, &r);\n";
			proxyMethodHandler << t3 << buildDeserializeCall("r", returnType, "rs").str();
			proxyMethodHandler << t3 << "return rs;\n";
		}

		proxyMethodHandler << t2 <<"});\n\n";
		proxyMethodHandler << t2 <<"return future;\n";
		proxyMethodHandler << t1 <<"}\n\n";
	}
}

static void buildStubImplVirtualMethods(Document& d)
{
	// Create the implementation method stubs
	for(auto& m : d["methods"].GetArray()) {
		string methodName = m["name"].GetString();
		string callType = m["callType"].GetString();

		// Create the method parameter list
		stringstream methodParameterList;

		bool first = true;
		for(auto& n : m["parameters"].GetArray()) {
			if(first)
				first = false;
			else
				methodParameterList << ", ";

			string type = n["type"].GetString();
			string name = n["name"].GetString();

			methodParameterList << type << " " << name;
		}

		// Get the return type
		string returnType = "void";
		if(m.HasMember("returnType"))
			returnType = m["returnType"].GetString();

		implMethodHandler << t1 << returnType << " " << methodName << "("
			<< methodParameterList.str() << ")\n";
		implMethodHandler << t1 << "{\n";
		implMethodHandler << t1 << "}\n\n";
	}
}

static void buildStubMessageEnumeratorAndCalls(Document& d)
{
	uint16_t methodCounter = 0;

	for(auto& m : d["methods"].GetArray()) {
		string methodName = m["name"].GetString();
		string callType = m["callType"].GetString();

		 // Create the method parameter list
		stringstream methodParameterList;

		bool first = true;
		for(auto& n : m["parameters"].GetArray()) {
			if(first)
				first = false;
			else
				methodParameterList << ", ";

			string type = n["type"].GetString();
			string name = n["name"].GetString();

			methodParameterList << type << " " << name;
		}

		// Get the return type
		string returnType = "void";
		if(m.HasMember("returnType"))
			returnType = m["returnType"].GetString();

		// Enumerate functions
		stubMethodEnumeration << t1 << "static const short METHOD_" << methodName
			<< "_CALL = "<< methodCounter << ";\n";

		// List calls
		stubMethodAbsFunc << t1 << "virtual " << returnType << " " << methodName << "("
			<< methodParameterList.str() << ") = 0;\n";

		++methodCounter;
	}
}

static void buildStubMessageHandlers(Document& d)
{
	// Create the stub message handler
	stubMethodHandler << t1 << "virtual void handleMessage(uint8_t messageType, uint16_t methodId, ByteStream& payload, ByteStream& result)"
		<< "\n" << t1 << "{\n";

	for(auto& m : d["methods"].GetArray()) {
		string methodName = m["name"].GetString();
		string callType = m["callType"].GetString();

		// Create the method call handler
		stubMethodHandler << t2 << "if(messageType == Message::" << callType << " && methodId == METHOD_" << methodName << "_CALL) {\n";

		 // Create the method parameter list
		stringstream methodParameterList;

		bool first = true;
		for(auto& n : m["parameters"].GetArray()) {
			if(first)
				first = false;
			else
				methodParameterList << ", ";

			string name = n["name"].GetString();

			methodParameterList << name;
		}

		// Get the return type
		string returnType = "void";
		if(m.HasMember("returnType"))
			returnType = m["returnType"].GetString();

		// Deserialize parameters from remote method call
		for(auto& n : m["parameters"].GetArray()) {

			string type = n["type"].GetString();
			string name = n["name"].GetString();

			stubMethodHandler << t3 << buildDeserializeCall("payload", type, name).str();
		}

		// Build method call with or without return type
		if(m.HasMember("returnType")) {
			string returnType = m["returnType"].GetString();
			stubMethodHandler << t3 << returnType << " ret = " << methodName << "(" << methodParameterList.str() << ");\n";
			stubMethodHandler << t3 << buildSerializeCall("result", returnType, "ret").str();
		} else {
			stubMethodHandler << t3 << methodName << "(" << methodParameterList.str() << ");\n";
		}

		stubMethodHandler << t2 << "}\n\n";
	}

	stubMethodHandler << t1 << "}\n";
}

static void parseServiceMethods(Document& d)
{
	// Create the proxy message handlers
	buildProxyMessageHandlers(d);

	// Enumerate the callable methods and
	// generate an abstract method for each one.
	buildStubMessageEnumeratorAndCalls(d);

	// Create the stub message handlers
	buildStubMessageHandlers(d);

	// Create the stub implementation virtual methods
	buildStubImplVirtualMethods(d);

	return;
}

static void buildStubClass()
{
	string serviceNameUpper(serviceName);
	std::transform(serviceNameUpper.begin(), serviceNameUpper.end(),
		serviceNameUpper.begin(), ::toupper);

	stubClass << "#ifndef " << serviceNameUpper << "_STUB_H\n";
	stubClass << "#define " << serviceNameUpper << "_STUB_H\n\n";
	stubClass << "using namespace ara::com::someip;\n\n";

	// Data structures go into the stub file
	stubClass << serviceDataStructuresCode.str() << '\n';

	stubClass << "class " << serviceName << "Stub : public ServiceStub\n";
	stubClass << "{\n";
	stubClass << "public:\n";
	stubClass << stubConstructor.str() << '\n';
	stubClass << stubMethodEnumeration.str() << '\n';
	stubClass << stubMethodAbsFunc.str() << '\n';
	stubClass << stubMethodHandler.str();
	stubClass << "};\n\n";

	stubClass << "#endif\n";
}

static void buildProxyClass()
{
	string serviceNameUpper(serviceName);
	std::transform(serviceNameUpper.begin(), serviceNameUpper.end(),
		serviceNameUpper.begin(), ::toupper);

	proxyClass << "#ifndef " << serviceNameUpper << "_PROXY_H\n";
	proxyClass << "#define " << serviceNameUpper << "_PROXY_H\n\n";

	proxyClass << "#include <future>\n\n";

	proxyClass << "using namespace ara::com::someip;\n\n";

	proxyClass << "class " << serviceName << "Proxy : public ServiceProxy\n";
	proxyClass << "{\n";
	proxyClass << "public:\n";
	proxyClass << proxyConstructor.str() << '\n';
	proxyClass << proxyMethodHandler.str();
	proxyClass << "};\n\n";

	proxyClass << "#endif\n";
}

static void buildImplClass()
{
	implClass << "class " << serviceName << "Impl : public "
		<< serviceName << "Stub\n";
	implClass << "{\n";
	implClass << "public:\n";
	implClass << implMethodHandler.str();
	implClass << "};\n";
}

/*
static const char* kTypeNames[] = 
	{ "Null", "False", "True", "Object", "Array", "String", "Number" };
printf("Type of member %s\n", kTypeNames[m.GetType()]);
*/

int main(int argc, char **argv)
{
	// Check provided arguments
	if(argc <= 2) {
		cout << "Please provide an input file and an output path." << endl;
		return EXIT_FAILURE;
	}

	// Open the file
	ifstream file(argv[1], ios::in|ios::binary|ios::ate);
	if(!file.is_open())
	{
		cerr << "The provided file cannot be opened or accessed." << endl;
		return EXIT_FAILURE;
	}

	// Get the directory name for output
	string path = argv[2];
	if(path.back() != '/') {
		cerr << "The last path character should be a /" << endl;
		return EXIT_FAILURE;
	}
	cout << "Writing output files to " << path << endl;

	// Read file to buffer json and close the file
	file.seekg(0, ios::end);
	streampos size = file.tellg();
	char *json = new char[size];
	file.seekg(0, ios::beg);
	file.read(json, size);
	file.close();

	// Parse a JSON string into DOM
	Document d;
	d.Parse(json);

	if(d.HasParseError()) {
		cerr << "\nError(offset" << (unsigned)d.GetErrorOffset() <<
			"): " << GetParseError_En(d.GetParseError()) << endl;
		return EXIT_FAILURE;
	}

	// Walk DOM and parse service data
	assert(d.IsObject());
	parseServiceAttributes(d);
	parseServiceDataStructures(d);
	parseServiceMethods(d);

	// Build stuff
	buildStubClass();
	buildProxyClass();
	buildImplClass();

	// Write the stub class
	stringstream stubFileName;
	stubFileName << path << serviceName << "Stub.h";
	ofstream stubFile(stubFileName.str());
	stubFile << stubClass.str();
	stubFile.close();

	// Write the proxy class
	stringstream proxyFileName;
	proxyFileName << path << serviceName << "Proxy.h";
	ofstream proxyFile(proxyFileName.str());
	proxyFile << proxyClass.str();
	proxyFile.close();

	// Write the implementation class
	stringstream implFileName;
	implFileName << path << serviceName << "Impl_Template.cc";
	ofstream implFile(implFileName.str());
	implFile << implClass.str();
	implFile.close();

	//  Tear down
	delete [] json;
	return EXIT_SUCCESS;
}
