/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <iostream>
#include <fstream>
#include <sstream>

#include <botan/hash.h>
#include <botan/hex.h>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include "rapidjson/ostreamwrapper.h"
#include "rapidjson/writer.h"
#include "rapidjson/reader.h"
#include "rapidjson/error/en.h"
#include "rapidjson/stringbuffer.h"

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"
#include "key_value_storage.h"

using namespace std;
using namespace ara::core;
using namespace rapidjson;

namespace ara::per {

void KeyValueStorage::ReadStorageFile(const ara::core::String& storageFileName)
{
	// Open the file
	ifstream file(storageFileName, ios::in|ios::binary);
	if(!file.is_open())
	{
		logger.LogError() << "The provided file "
			<< storageFileName << " cannot be opened or accessed";

		// Initialize document as an (empty) object
		d.SetObject();

		return;
	}

	IStreamWrapper isw(file);

	// Parse a JSON Stream into DOM
	d.ParseStream(isw);

	if(d.HasParseError()) {
		logger.LogError()  << "Error at offset " << (unsigned)d.GetErrorOffset()
			<< "): " << GetParseError_En(d.GetParseError());
		return;
	}

	// Walk DOM and parse service data
	assert(d.IsObject());
}

void KeyValueStorage::WriteStorageFile(const ara::core::String& storageFileName) const 
{
	// Open the file
	ofstream file(storageFileName, ios::out|ios::binary);
	if(!file.is_open())
	{
		logger.LogError() << "The provided file "
		    << storageFileName << " cannot be opened or accessed";
		return;
	}

	OStreamWrapper osw(file);
	Writer<OStreamWrapper> writer(osw);
	d.Accept(writer);
	file.close();

	logger.LogInfo() << "The storage file was written";
}

static StringView hexifyDigest(unsigned char* digest)
{
	char md5string[33];

	for(int i = 0; i < 16; ++i)
		sprintf(&md5string[i*2], "%02x", (unsigned int) digest[i]);

	return StringView(md5string);
}

static bool compareDigests(unsigned char* digest1, unsigned char *digest2)
{
	for(int i = 0; i < 16; ++i)
		if(digest1[i] != digest2[i])
			return 0;

	return 1;
}

void KeyValueStorage::SafelyWriteStorageFile(const ara::core::String& storageFileName) const
{
	StringView newStorageFileName = storageFileName + ".new";
	StringView crcNewStorageFileName = storageFileName + ".crc";

	// Save file with ".new" extension
	WriteStorageFile(newStorageFileName);
	logger.LogInfo() << "Wrote " << newStorageFileName;

	// Generate the CRC of whatever was written to disk and save that
	// to file with ".crc" extention
	FILE *f = fopen(newStorageFileName.c_str(), "rb");
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	char str[fsize];
	fseek(f, 0, SEEK_SET);
	fread(str, 1, fsize, f);
	fclose(f);

	// Generate the CRC from read data
	std::unique_ptr<Botan::HashFunction> hash(Botan::HashFunction::create("MD5"));
	hash->update((const uint8_t*) str, fsize);
	uint8_t digest[hash->output_length()];
	hash->final(digest);

	FILE *fCRC = fopen(crcNewStorageFileName.c_str(), "wb");
	fwrite(digest, 1, 16, fCRC);
	fclose(fCRC);

	logger.LogInfo() << "Calculated and stored MD5 " << Botan::hex_encode(digest, hash->output_length())
		<< " for " << newStorageFileName;

	// Now overwrite the original storage file with the new one
	rename(newStorageFileName.c_str(), storageFileName.c_str());

	logger.LogInfo() << "Overwrote original storage file";
}

void KeyValueStorage::SafelyReadStorageFile(const ara::core::String& storageFileName)
{
	StringView crcStorageFileName = storageFileName + ".crc";

	// Read storage file
	FILE *f = fopen(storageFileName.c_str(), "rb");

	if(f == NULL) {
		logger.LogError() << "The provided file " << storageFileName
			<< " cannot be opened or accessed";

		// Initialize document as an (empty) object
		d.SetObject();

		return;
	}

	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	char str[fsize];
	fseek(f, 0, SEEK_SET);
	fread(str, 1, fsize, f);
	fclose(f);

	// Generate the CRC from read data
	std::unique_ptr<Botan::HashFunction> hash(Botan::HashFunction::create("MD5"));
	hash->update((const uint8_t*) str, fsize);
	uint8_t digestRead[hash->output_length()];
	hash->final(digestRead);

	logger.LogInfo() << "Calculated MD5 " << Botan::hex_encode(digestRead, hash->output_length())
		<< " for " << storageFileName;

	// Read the CRC file
	unsigned char digestStored[16];
	FILE *fCRC = fopen(crcStorageFileName.c_str(), "rb");
	fread(digestStored, 1, 16, fCRC);
	fclose(fCRC);

	logger.LogInfo() << "Read MD5 " << Botan::hex_encode(digestStored, hash->output_length())
		<< " from " << crcStorageFileName;

	// Parse the storage
	// This could also be passed from str above
	ReadStorageFile(storageFileName);

	// Now compare generated digest and stored digest
	if(!memcmp(digestRead, digestStored, hash->output_length())) {
		logger.LogWarn() << "Mismatch in generated digest of file and stored digest";
		// What now...
	}
}

ara::core::Result<std::unique_ptr<KeyValueStorage> >
	CreateKeyValueStorage (ara::core::StringView database) noexcept
{
	std::unique_ptr<KeyValueStorage> kvs(new KeyValueStorage(database));
	ara::core::Result<std::unique_ptr<KeyValueStorage> > res(std::move(kvs));
	return res;
}

ara::core::Result<void> 
	RecoverKeyValueStorage (ara::core::StringView database) noexcept
{

}

ara::core::Result<void>
	ResetKeyValueStorage (ara::core::StringView database) noexcept
{

}

}
