/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef KEY_VALUE_STORAGE_H
#define KEY_VALUE_STORAGE_H

#include <memory>

#include "rapidjson/document.h"
#include "rapidjson/reader.h"
#include "rapidjson/error/en.h"
#include "rapidjson/stringbuffer.h"

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"

using namespace rapidjson;

namespace ara::per {

// See [SWS_PER_00339]
class KeyValueStorage
{
public:
	// TODO This is missing from the specification or is the object instance
	// supposed to be created differently somehow?
	KeyValueStorage (ara::core::StringView &storageFileName) noexcept
	{
		this->storageFileName = storageFileName;
		//ReadStorageFile(storageFileName);
		SafelyReadStorageFile(storageFileName);
	};

	// See [SWS_PER_00322]
	KeyValueStorage (KeyValueStorage &&kvs) noexcept;

	// See [SWS_PER_00324]
	KeyValueStorage (const KeyValueStorage &)=delete;

	// See [SWS_PER_00323]
	KeyValueStorage& operator= (KeyValueStorage &&kvs) noexcept;

	// See [SWS_PER_00050]
	~KeyValueStorage () noexcept 
	{
		// Do not write a modified persistency
		// See [SWS_PER_00331]
	}

	// See [SWS_PER_00042]
	ara::core::Vector<ara::core::String> GetAllKeys () const noexcept;

	// See [SWS_PER_00043]
	bool HasKey (ara::core::StringView key) const noexcept
	{
		if(d.HasMember(key.c_str()))
		    return true;

		return false;
	}

	// See [SWS_PER_00044]
	template <class T> ara::core::Result<void> 
		GetValue (ara::core::StringView key, T &value) const noexcept;

	// See [SWS_PER_00332]
	template <class T> ara::core::Result<T>
		GetValue (ara::core::StringView key) const noexcept
	{
		if(d.HasMember(key.c_str())) {
			auto str = ara::core::StringView(d[key.c_str()].GetString());
			return ara::core::Result<T>(str);
		}

		// This should cause an exception, HasKey should have been used first
		logger.LogError() << "Requested non-existent key. Please use HasKey(). Returning non-conclusive result";
		return ara::core::Result<T>("NULL");
	}

	// See [SWS_PER_00046]
	template <class T> ara::core::Result<void>
		SetValue (ara::core::StringView key, const T&value) noexcept
	{
		logger.LogError() << "Type-agnostic SetValue method not implemented";
		return ara::core::Result<void>();
	}

	ara::core::Result<void>
		SetValue (ara::core::StringView key, const ara::core::StringView& value) noexcept 
	{
		Value vkey(key.c_str(), d.GetAllocator());
		Value vval(value.c_str(), d.GetAllocator());

		d.RemoveMember(vkey);
		d.AddMember(vkey, vval, d.GetAllocator());

		documentModifed = true;

		return ara::core::Result<void>();
	}

	ara::core::Result<void>
		SetValue (ara::core::StringView key, char const* value) noexcept
	{
		Value vkey(key.c_str(), d.GetAllocator());
		Value vval(value, d.GetAllocator());

		if(d.HasMember(vkey))
			d.RemoveMember(vkey);
		d.AddMember(vkey, vval, d.GetAllocator());

		documentModifed = true;

		return ara::core::Result<void>();
	}

	// See [SWS_PER_00047]
	ara::core::Result<void> RemoveKey (ara::core::StringView key) noexcept;

	// See [SWS_PER_00048]
	ara::core::Result<void> RemoveAllKeys () noexcept;

	// See [SWS_PER_00049]
	ara::core::Result<void> SyncToStorage () const noexcept
	{
		if(documentModifed)
			SafelyWriteStorageFile(storageFileName);

		return ara::core::Result<void>();
	}

private:
	ara::log::Logger& logger = ara::log::CreateLogger ("", "KeyValueStorage");

	void ReadStorageFile(const ara::core::String& storageFileName);
	void WriteStorageFile(const ara::core::String& storageFileName) const;
	void SafelyWriteStorageFile(const ara::core::String& storageFileName) const;
	void SafelyReadStorageFile(const ara::core::String& storageFileName);

	ara::core::String storageFileName = "storage_file";

	Document d;
	bool documentModifed = false;
};

// See [SWS_PER_00052]
ara::core::Result<std::unique_ptr<KeyValueStorage> >
	CreateKeyValueStorage (ara::core::StringView database) noexcept;

// See [SWS_PER_00333]
ara::core::Result<void>
	RecoverKeyValueStorage (ara::core::StringView database) noexcept;

// See [SWS_PER_00334]
ara::core::Result<void>
	ResetKeyValueStorage (ara::core::StringView database) noexcept;

}

#endif
