/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef ERROR_H
#define ERROR_H

#include "ara/log/logstream.h"

namespace ara::per {

// See [SWS_PER_00311]
enum PerErrc {
	kStorageLocationNotFoundError = 1, // Requested storage location is not found or not configured in the AUTOSAR model.
	kKeyNotFoundError = 2, // The key was not found.
	kIllegalWriteAccessError = 3, // Opening the resource for writing failed because it is configured read-only.
	kPhysicalStorageError = 4, // A severe error which might happen during the operation, such as out of memory or writing/reading to the storage return an error.
	kIntegrityError= 5, // The integrity of the storage could not be established. This can happen when the structure of a key value database is corrupted, or a read-only file has no content.
	kValidationError= 6, // The validation of redundancy measures failed for a single key, for the whole key value data base, or for a file.
	kEncryptionError= 7, // The encryption or decryption failed for a single key, for the whole key value data base, or for a file.
	kDataTypeMismatchError= 8, // The provided data type does not match the stored data type.
	kInitValueNotAvailableError = 9, // The operation could not be performed because no initial value is available.
	kResourceBusyError= 10, // The operation could not be performed because the resource is currently busy.
	kInternalError= 11
};

// See [SWS_PER_00312]
class PerErrorDomain : public ErrorDomain
{
	// See [SWS_PER_00316]
	const ErrorDomain::IdType;

	// See [SWS_PER_00313]
	PerErrorDomain () noexcept;

	// See [SWS_PER_00314]
	char const* Name () const noexcept override;

	// See [SWS_PER_00315]
	char const* Message (CodeType errorCode) const noexcept override;
};

#endif
