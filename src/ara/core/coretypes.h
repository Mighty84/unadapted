/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef CORE_TYPES_H
#define CORE_TYPES_H

#include <string>
#include <vector>
#include <system_error>

namespace ara::core {

// See [SWS_CORE_00040]
// See [SWS_CORE_02001]
using StringView = std::string;

// See [SWS_CORE_00040]
// See [SWS_CORE_03001]
using String = std::string;

// See [SWS_CORE_00040]
// See [SWS_CORE_01301]
template<class T>
using Vector = std::vector<T>;

using ErrorCode = std::error_code;

}

#include "result.h"

#endif
