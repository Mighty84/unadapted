/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef RESULT_H
#define RESULT_H

#include <string>
#include <vector>
#include <system_error>

namespace ara::core {

// See [SWS_CORE_00701]
template <typename T, typename E = ErrorCode>
class Result
{
public:
	// See [SWS_CORE_00711]
	using value_type = T;

	// See [SWS_CORE_00712]
	using error_type = E;

	// See [SWS_CORE_00721]
	Result (T const &t) {
		value = t;
	}

	// See [SWS_CORE_00722]
	Result (T &&t) {
		value = std::move(t);
	}

	// See [SWS_CORE_00723]
	explicit Result (E const &e);

	// See [SWS_CORE_00724]
	explicit Result (E &&e);

	// See [SWS_CORE_00725]
	Result (Result const &other);

	// See [SWS_CORE_00726]
	Result (Result &&other);

	// See [SWS_CORE_00731]
	static Result FromValue (T const &t);

	// See [SWS_CORE_00732]
	static Result FromValue (T &&t);

	// See [SWS_CORE_00733]
	template <typename... Args> static Result FromValue (Args &&...args);

	// See [SWS_CORE_00734]
	static Result FromError (E const &e);

	// See [SWS_CORE_00735]
	static Result FromError (E &&e);

	// See [SWS_CORE_00736]
	template <typename... Args> static Result FromError (Args &&...args);

	// See [SWS_CORE_00741]
	Result& operator= (Result const &other);

	// See [SWS_CORE_00742]
	Result& operator= (Result &&other);

	// See [SWS_CORE_00743]
	template <typename... Args> void EmplaceValue (Args &&...args);

	// See [SWS_CORE_00744]
	template <typename... Args> void EmplaceError (Args &&...args);

	// See [SWS_CORE_00745]
	void Swap (Result &other) noexcept(std::is_nothrow_move_constructible< T >::value &&std::is_nothrow_move_assignable< T >::value);

	// See [SWS_CORE_00751]
	bool HasValue () const noexcept;

	// See [SWS_CORE_00752]
	explicit operator bool () const noexcept;

	// See [SWS_CORE_00753]
	T const& operator* () const ;

	// See [SWS_CORE_00754]
	T const* operator-> () const ;

	// See [SWS_CORE_00755]
	T const& Value () const & 
	{
		return value;
	}

	// See [SWS_CORE_00756]
	T&& Value ()&& ;

	// See [SWS_CORE_00757]
	E const& Error () const &;

	// See [SWS_CORE_00758]
	E&& Error ()&&;

	// See [SWS_CORE_00761]
	template <typename U> T ValueOr (U &&defaultValue) const &;

	// See [SWS_CORE_00762]
	template <typename U> T ValueOr (U &&defaultValue)&&;

	// See [SWS_CORE_00763]
	template <typename G> E ErrorOr (G &&defaultError) const ;

	// See [SWS_CORE_00765]
	template <typename G> bool CheckError (G &&error) const ;

	// See [SWS_CORE_00766]
	T const& ValueOrThrow () const noexcept(false);

	// See [SWS_CORE_00767]
	template <typename F> T Resolve (F &&f) const ;

	// See [SWS_CORE_00768]
	//template <typename F> auto Bind (F &&f) const -> SEE_BELOW;

	// See [SWS_CORE_00796]
	//template <typename T, typename E> void swap (Result< T, E > &lhs, Result< T, E > &rhs) noexcept(noexcept(lhs.Swap(rhs)));

private:
	T value;
};

// Partial specialization for void type maps to int
template <>
class Result<void> : Result<int>
{
public:
	Result() : Result<int>(0) {}
};

}

#endif
