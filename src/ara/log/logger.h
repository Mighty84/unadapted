/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef LOGGER_H
#define LOGGER_H

#include "logstream.h"

namespace ara::log {

class Logger
{
public:
	// See [SWS_LOG_00064]
	LogStream LogFatal () noexcept;

	// See [SWS_LOG_00065]
	LogStream LogError () noexcept;

	// See [SWS_LOG_00066]
	LogStream LogWarn () noexcept;

	// See [SWS_LOG_00067]
	LogStream LogInfo () noexcept;

	// See [SWS_LOG_00068]
	LogStream LogDebug () noexcept;

	// See [SWS_LOG_00069]
	LogStream LogVerbose () noexcept;

	// See [SWS_LOG_00070]
	bool IsEnabled (LogLevel logLevel) const noexcept;

private:
	LogStream fatal;
	LogStream error;
	LogStream warn;
	LogStream info;
	LogStream debug;
	LogStream verbose;
};

}

#endif
