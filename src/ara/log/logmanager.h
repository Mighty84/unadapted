/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef LOGMANAGER_H
#define LOGMANAGER_H

namespace ara::log {

// See [SWS_LOG_00097]
class LogManager
{
public:
	// See [SWS_LOG_00100]
	static Logger& CreateLogContext (ara::core::StringView ctxId,
		ara::core::StringView ctxDescription, LogLevel ctxDefLogLevel)
		noexcept;

	// See [SWS_LOG_00103]
	~LogManager ();

	// See [SWS_LOG_00104]
	LogManager (const LogManager &)=delete;

	// See [SWS_LOG_00105]
	LogManager (LogManager &&)=delete;

	// See [SWS_LOG_00106]
	LogManager& operator= (const LogManager &)=delete;

	// See [SWS_LOG_00107]
	LogManager& operator= (LogManager &&)=delete;

	// See [SWS_LOG_00098]
	enum ClientState {
		kUnknown= -1;
		kNotConnected,
		kConnected
	}

	// See [SWS_LOG_00101]
	ClientState RemoteClientState () const noexcept;
};

}

#endif
