/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef COMMON_H
#define COMMON_H

namespace ara::log {

// See [SWS_LOG_00018]
enum LogLevel {
	kOff = 0, // No logging.
	kFatal = 1, // Fatal error.
	kError = 2, // Error with impact to correct functionality.
	kWarn = 3, // Warning if correct behavior cannot be ensured.
	kInfo = 4, // Informational, high level understanding.
	kDebug = 5, // Detailed information for programmers.
	kVerbose = 6 // Extra-verbose debug messages.
};

// See [SWS_LOG_00019]
enum LogMode {
	kRemote = 0x01, // Send remotely.
	kFile = 0x02, // Save to file.
	kConsole = 0x04 // Forward to console output.
};

// See [SWS_LOG_00108]
struct LogHex8 {
	uint8_t value;
};

// See [SWS_LOG_00109]
struct LogHex16 {
	uint16_t value;
};

// See [SWS_LOG_00110]
struct LogHex32 {
	uint32_t value;
};

// See [SWS_LOG_00111]
struct LogHex64 {
	uint64_t value;
};

// See [SWS_LOG_00112]
struct LogBin8 {
	uint8_t value;
};

// See [SWS_LOG_00113]
struct LogBin16 {
	uint16_t value;
};

// See [SWS_LOG_00114]
struct LogBin32 {
	uint32_t value;
};

// See [SWS_LOG_00115]
struct LogBin64 {
	uint64_t value;
};

// See [SWS_LOG_00116]
struct LogRawBuffer {
	void const * buffer;
	uint16_t size;
};

}

#endif
