/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef LOGGING_H
#define LOGGING_H

#include "ara/core/coretypes.h"
#include "common.h"
#include "logger.h"

// Forward declarations
namespace ara::log { class Logger; }

namespace ara::log {

// See [SWS_LOG_00021], [SWS_LOG_00005]
Logger& CreateLogger (ara::core::StringView ctxId,
	ara::core::StringView ctxDescription, LogLevel appDefLogLevel=kWarn);

// See [SWS_LOG_00022]
LogHex8 HexFormat (uint8_t value) noexcept;

// See [SWS_LOG_00023]
LogHex8 HexFormat (int8_t value) noexcept;

// See [SWS_LOG_00024]
LogHex16 HexFormat (uint16_t value) noexcept;

// See [SWS_LOG_00025]
LogHex16 HexFormat (int16_t value) noexcept;

// See [SWS_LOG_00026]
LogHex16 HexFormat (uint32_t value) noexcept;

// See [SWS_LOG_00027]
LogHex32 HexFormat (int32_t value) noexcept;

// See [SWS_LOG_00028]
LogHex32 HexFormat (uint64_t value) noexcept;

// See [SWS_LOG_00029]
LogHex64 HexFormat (int64_t value) noexcept;

// See [SWS_LOG_00030]
LogBin8 BinFormat (uint8_t value) noexcept;

// See [SWS_LOG_00031]
LogBin8 BinFormat (int8_t value) noexcept;

// See [SWS_LOG_00032]
LogBin16 BinFormat (uint16_t value) noexcept;

// See [SWS_LOG_00033]
LogBin16 BinFormat (int16_t value) noexcept;

// See [SWS_LOG_00034]
LogBin16 BinFormat (uint32_t value) noexcept;

// See [SWS_LOG_00035]
LogBin32 BinFormat (int32_t value) noexcept;

// See [SWS_LOG_00036]
LogBin32 BinFormat (uint64_t value) noexcept;

// See [SWS_LOG_00037]
LogBin64 BinFormat (int64_t value) noexcept;

// See [SWS_LOG_00038]
template <typename T, typename std::enable_if<!std::is_pointer< T >::value, std::nullptr_t >::type>
	constexpr LogRawBuffer RawBuffer (const T &value) noexcept;

}

#endif
