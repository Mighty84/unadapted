/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <iostream>

#include <stdint.h>
#include <stdio.h>

#include "ara/core/coretypes.h"
#include "logging.h"

namespace ara::log {

LogStream::~LogStream()
{
	printf("\n");
}

void LogStream::Flush () noexcept
{
	fflush(stdout);
}

LogStream& LogStream::operator<< (bool value) noexcept
{
	printf("%u", value);
	return *this;
}

LogStream& LogStream::operator<< (uint8_t value) noexcept
{
	printf("%u", value);
	return *this;
}

LogStream& LogStream::operator<< (uint16_t value) noexcept
{
	printf("%u", value);
	return *this;
}

LogStream& LogStream::operator<< (uint32_t value) noexcept
{
	printf("%u", value);
	return *this;
}

LogStream& LogStream::operator<< (int8_t value) noexcept
{
	printf("%i", value);
	return *this;
}

LogStream& LogStream::operator<< (int16_t value) noexcept
{
	printf("%i", value);
	return *this;
}

LogStream& LogStream::operator<< (int32_t value) noexcept
{
	printf("%i", value);
	return *this;
}

LogStream& LogStream::operator<< (int64_t value) noexcept
{
	printf("%li", value);
	return *this;
}

LogStream& LogStream::operator<< (ara::core::StringView value) noexcept
{
	printf("%s", value.c_str());
	return *this;
}

LogStream& LogStream::operator<< (char const* value) noexcept
{
	printf("%s", value);
	return *this;
}


}
