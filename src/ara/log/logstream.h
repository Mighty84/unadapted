/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef LOGSTREAM_H
#define LOGSTREAM_H

#include <iostream>

#include <stdint.h>

#include "ara/core/coretypes.h"
#include "logging.h"

namespace ara::log {

class LogStream
{
public:
	~LogStream();

	// See [SWS_LOG_00039]
	void Flush () noexcept;

	// See [SWS_LOG_00040]
	LogStream& operator<< (bool value) noexcept;

	// See [SWS_LOG_00041]
	LogStream& operator<< (uint8_t value) noexcept;

	// See [SWS_LOG_00042]
	LogStream& operator<< (uint16_t value) noexcept;

	// See [SWS_LOG_00043]
	LogStream& operator<< (uint32_t value) noexcept;

	// See [SWS_LOG_00044]
	LogStream& operator<< (uint64_t value) noexcept;

	// See [SWS_LOG_00045]
	LogStream& operator<< (int8_t value) noexcept;

	// See [SWS_LOG_00046]
	LogStream& operator<< (int16_t value) noexcept;

	// See [SWS_LOG_00047]
	LogStream& operator<< (int32_t value) noexcept;

	// See [SWS_LOG_00048]
	LogStream& operator<< (int64_t value) noexcept;

	// See [SWS_LOG_00049]
	LogStream& operator<< (float value) noexcept;

	// See [SWS_LOG_00050]
	LogStream& operator<< (double value) noexcept;

	// See [SWS_LOG_00053]
	LogStream& operator<< (const LogHex8 &value) noexcept;

	// See [SWS_LOG_00054]
	LogStream& operator<< (const LogHex16 &value) noexcept;

	// See [SWS_LOG_00055]
	LogStream& operator<< (const LogHex32 &value) noexcept;

	// See [SWS_LOG_00056]
	LogStream& operator<< (const LogHex64 &value) noexcept;

	// See [SWS_LOG_00057]
	LogStream& operator<< (const LogBin8 &value) noexcept;

	// See [SWS_LOG_00058]
	LogStream& operator<< (const LogBin16 &value) noexcept;

	// See [SWS_LOG_00059]
	LogStream& operator<< (const LogBin32 &value) noexcept;

	// See [SWS_LOG_00060]
	LogStream& operator<< (const LogBin64 &value) noexcept;

	// See [SWS_LOG_00061]
	LogStream& operator<< (const LogRawBuffer &value) noexcept;

	// See [SWS_LOG_00062]
	LogStream& operator<< (ara::core::StringView value) noexcept;

	// Needed for regular static strings of type "const char *"
	LogStream& operator<< (char const* value) noexcept;

	// See [SWS_LOG_00063]
	LogStream& operator<< (LogLevel value) noexcept;
};

}

#endif
