/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <thread>
#include <unistd.h>

#include "ara/core/coretypes.h"
#include "ara/log/logger.h"
#include "ara/log/logging.h"
#include "ara/per/key_value_storage.h"
#include "ara/com/someip/rpc/ServiceRegistry.h"

#include "HelloWorldServiceStub.h"
#include "HelloWorldServiceProxy.h"

using namespace ara::core;
using namespace ara::com;
using namespace ara::com::someip;
using namespace ara::log;
using namespace ara::per;

// Based on HelloWorldServiceImpl_Template.cc file
class HelloWorldServiceImpl : public HelloWorldServiceStub
{
public:
	string hello(string GreetingText)
	{
		printf("hello method called!\n");
		return string("hello method received message: " + GreetingText);
	}

	void testmethod(uint8_t CallCounter, uint8_t CRC) {

	}
};

int main(int argc, char *argv[])
{
	Logger& logger = CreateLogger ("HelloWorldDemo", "");
	logger.LogInfo() << "Starting the application"s;

	// Create and start a service registry
	StringView addr("224.224.224.0");
	ServiceRegistry serviceRegistry(addr, 30490, 5000);

	// Load persistency
	logger.LogInfo() << "Loading persistence file";
	Result< std::unique_ptr<KeyValueStorage> > res1 = CreateKeyValueStorage("persistence.json");
	auto& kvs = res1.Value();

	// Test Persistence with key/value
	StringView key = "testkey";
	StringView value = "testvalue";

	// Check if the key exists
	logger.LogInfo() << "Checking for existence of testkey";
	if(kvs->HasKey(key))
		logger.LogInfo() << "Key " << key << " exists in persistence file";
	else
		logger.LogInfo() << "Key " << key << " does not exist in persistence file";

	// Read a key (both existing and non-existing paths should work)
	logger.LogInfo() << "Reading a test key";
	Result<StringView> res2 = kvs->GetValue<StringView>(key);
	auto &val = res2.Value();
	logger.LogInfo() << "Read value from persistence key " << key << " : " << val;

	// Write a key
	logger.LogInfo() << "Writing a test key";
	Result<void> res3 = kvs->SetValue(key, value);

	kvs->SyncToStorage();
	logger.LogInfo() << "Triggered sync to storage";

	// Create a service implementation that will
	// handle all called service methods
	HelloWorldServiceImpl helloStub;
	serviceRegistry.registerService(helloStub);

	// Create a service proxy
	HelloWorldServiceProxy helloProxy;
	serviceRegistry.registerService(helloProxy);

	// Call the hello method periodically
	while(true) {
		std::future<string> resp = helloProxy.hello("Test string");
		if(resp.valid())
			logger.LogInfo() << "Response: " << resp.get();

		helloStub.handleMessages();

		serviceRegistry.update();

		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	return EXIT_SUCCESS;
}
